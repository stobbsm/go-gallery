package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"

	"gitlab.com/stobbsm/go-gallery/api/config"
	"launchpad.net/go-xdg"
)

var (
	servepath, dbhost, dbname, dbusername, servehost string
	dbpassword, fstype, fspath, configPath           string
	dbport, serveport                                int
)

func init() {
	flag.IntVar(&dbport, "port", 5432, "Set the port used by the database")
	flag.StringVar(&dbhost, "host", "localhost", "Set the database hostname")
	flag.StringVar(&dbname, "name", "gallery", "Set the database name")
	flag.StringVar(&dbusername, "username", "gallery", "Set the database username")
	flag.StringVar(&dbpassword, "password", "", "Set the database connection password")
	flag.StringVar(&fstype, "storetype", "filesystem", "Set the storage type")
	flag.StringVar(&fspath, "storepath", "/opt/gallery/storage", "Set the storage path")
	flag.StringVar(&configPath, "configpath", xdg.Config.Home()+"/gallery/config.json", "Configurtion path")
	flag.StringVar(&servepath, "servepath", "/srv/go-gallery/web", "Path to server web interface on")
	flag.StringVar(&servehost, "servehost", "localhost", "Hostname of the web server")
	flag.IntVar(&serveport, "servePort", 8888, "Port to server web interface on")
}

func main() {
	flag.Parse()
	c := config.CreateConfig(dbhost, dbname, dbusername, dbpassword, fstype, fspath, servehost, servepath, serveport, dbport)
	js, err := c.DumpJSON()
	if err != nil {
		panic(err)
	}

	configDir := filepath.Dir(configPath)
	if err := os.MkdirAll(configDir, 0755); err != nil {
		panic(err)
	}

	cfile, err := os.Create(configPath)
	if err != nil {
		panic(err)
	}
	defer cfile.Close()
	cfile.Chmod(0600)
	fmt.Fprintf(cfile, string(js))
	log.Println("Configuration created at ", configPath)
}
