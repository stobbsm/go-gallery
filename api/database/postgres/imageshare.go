package postgres

import (
	"errors"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/models"
	q "gitlab.com/stobbsm/go-querybuilder"
)

// Queries needed
var (
	checkShareExistsQuery = q.QueryBuilder(
		q.Select(
			`image_share.id`,
		),
		q.From(`image_share`),
		q.WhereEq(`image_share.image_id`),
		q.AndEq(`image_share.user_id`),
	)
	createImageShare = q.QueryBuilder(
		q.Insert(
			`image_share`,
			`id`,
			`image_id`,
			`user_id`,
			`shared`,
			`created_at`,
			`updated_at`,
		),
	)
	getSharedImagesByUserID = q.QueryBuilder(
		selectAllImage,
		q.From(`images`),
		q.InnerJoin(`image_share`),
		q.On(`images.id`, `image_share.image_id`),
		q.InnerJoin(`users`),
		q.On(`users.id`, `image_share.user_id`),
		q.WhereEq(`users.id`),
		q.AndEq(`image_share.shared`),
	)
	getSharedUsersByImageID = q.QueryBuilder(
		prepareSelectUser,
		prepareFromUser,
		q.InnerJoin(`users`),
		q.On(`image_share.user_id`, `users.id`),
		q.InnerJoin(`images`),
		q.On(`image_share.image_id`, `images.id`),
		q.WhereEq(`images.id`),
		q.AndEq(`image_share.shared`),
	)
)

// GetSharedImagesByUserID returns the list of images by the UserID
func (d *Db) GetSharedImagesByUserID(userid uuid.UUID) ([]models.Image, error) {
	images, err := d.GetImageByQuery(getSharedImagesByUserID, userid, true)
	if err != nil {
		return nil, err
	}
	return images, nil
}

func (d *Db) getSharedUsersByImageID(imageid uuid.UUID) ([]models.User, error) {
	users, err := d.GetUserByQuery(getSharedUsersByImageID, imageid, true)
	if err != nil {
		return nil, err
	}
	return users, nil
}

// checkShareExists checks for the existence of a share
func (d *Db) checkShareExists(imageid, userid uuid.UUID) error {
	stmt, err := d.Prepare(checkShareExistsQuery)
	defer stmt.Close()
	if err != nil {
		return NewUnknownError(err)
	}

	rows, err := stmt.Query(imageid, userid)
	defer rows.Close()
	if err != nil {
		return NewUnknownError(err)
	}

	var s models.ImageShare
	for rows.Next() {
		log.Println(`Scanning share id`)
		err = rows.Scan(
			&s.ID,
		)
		if err != nil {
			return NewModelNotFound(err)
		}
	}
	if s.ID == uuid.Nil {
		return NewModelNotFound(errors.New(`Share not found`))
	}
	return NewModelFound(errors.New(`Share Exists`))
}

// ShareImage inserts an image share
func (d *Db) ShareImage(imageshare *models.ImageShare) error {
	if err := d.checkShareExists(imageshare.ImageID, imageshare.UserID); err != nil {
		switch err.(type) {
		case *ModelNotFound:
			log.Println(err)
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	stmt, err := d.Prepare(createImageShare)
	defer stmt.Close()
	if err != nil {
		return NewUnknownError(err)
	}

	imageshare.ID = models.GenUUID()
	imageshare.CreatedAt.Set(time.Now())
	imageshare.UpdatedAt = imageshare.CreatedAt

	_, err = stmt.Exec(
		imageshare.ID,
		imageshare.ImageID,
		imageshare.UserID,
		imageshare.Shared,
		imageshare.CreatedAt.GetTime(),
		imageshare.UpdatedAt.GetTime(),
	)
	if err != nil {
		return NewUnknownError(err)
	}

	return nil
}
