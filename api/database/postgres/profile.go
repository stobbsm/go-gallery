package postgres

import (
	"errors"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/models"
	q "gitlab.com/stobbsm/go-querybuilder"
)

var (
	selectAllProfilePrepare = q.Prepare(q.Select("id", "user_id", "background", "profile", "created_at", "updated_at"))
	createProfileQuery      = q.QueryBuilder(
		q.Insert("profiles", "id", "user_id", "background", "profile", "created_at", "updated_at"),
	)
	getProfileByIDQuery = q.QueryBuilder(
		selectAllProfilePrepare,
		q.From("profiles"),
		q.WhereEq("id"),
	)
	getProfileByUserIDQuery = q.QueryBuilder(
		selectAllProfilePrepare,
		q.From("profiles"),
		q.WhereEq("user_id"),
	)
	updateProfileQuery = q.QueryBuilder(
		q.Update("profiles"),
		q.Set("background", "profile", "updated_at"),
		q.WhereEq("id"),
	)
	deleteProfileQuery = q.QueryBuilder(
		q.Delete,
		q.From("profiles"),
		q.WhereEq("id"),
	)
)

// GetProfileByID retrieves a profile via it's ID
func (d *Db) GetProfileByID(id uuid.UUID) (*models.Profile, error) {
	p, err := d.GetProfileByQuery(getProfileByIDQuery, id)
	if err != nil {
		return nil, err
	}
	return &p[0], err
}

// GetProfileByUserID retrieves a profile via it's User ID association
func (d *Db) GetProfileByUserID(userid uuid.UUID) (*models.Profile, error) {
	p, err := d.GetProfileByQuery(getProfileByUserIDQuery, userid)
	if err != nil {
		switch err.(type) {
		case *ModelNotFound:
			return nil, err
		default:
			return nil, NewUnknownError(err)
		}
	}
	if len(p) == 0 {
		return nil, NewModelNotFound(errors.New(`Profile not found`))
	}
	return &p[0], nil
}

// GetProfileByQuery returns a list of profiles matching the query and given arguments.
func (d *Db) GetProfileByQuery(query string, args ...interface{}) ([]models.Profile, error) {
	stmt, err := d.Prepare(query)
	if err != nil {
		return nil, NewUnknownError(err)
	}
	defer stmt.Close()

	r, err := stmt.Query(args...)
	if err != nil {
		return nil, NewUnknownError(err)
	}
	defer r.Close()

	var profiles []models.Profile
	for r.Next() {
		var createdat, updatedat time.Time
		var p models.Profile
		err = r.Scan(
			&p.ID,
			&p.UserID,
			&p.BackgroundID,
			&p.ProfileID,
			&createdat,
			&updatedat,
		)
		if err != nil {
			return profiles, NewModelNotFound(err)
		}

		profiles = append(profiles, p)
	}

	return profiles, nil
}

func (d *Db) checkIfProfileExists(userid uuid.UUID) error {
	p, err := d.GetProfileByQuery(getProfileByUserIDQuery, userid)
	if err != nil {
		switch err.(type) {
		case *ModelNotFound:
			return err
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	if len(p) == 0 {
		return NewModelNotFound(errors.New(`Profile not found`))
	}
	return NewModelFound(nil)
}

// CreateProfile Inserts a profile into the database
func (d *Db) CreateProfile(p *models.Profile) error {
	if err := d.checkIfProfileExists(p.UserID); err != nil {
		switch err.(type) {
		case *ModelNotFound:
			log.Println(err)
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}

	p.ID = models.GenUUID()
	p.CreatedAt.Set(time.Now())
	p.UpdatedAt = p.CreatedAt

	stmt, err := d.Prepare(createProfileQuery)
	if err != nil {
		return NewUnknownError(err)
	}
	defer stmt.Close()

	_, err = stmt.Exec(
		p.ID,
		p.UserID,
		p.BackgroundID,
		p.ProfileID,
		p.CreatedAt.GetTime(),
		p.UpdatedAt.GetTime(),
	)
	if err != nil {
		return NewUnknownError(err)
	}

	return nil
}
