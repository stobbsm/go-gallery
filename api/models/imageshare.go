package models

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// ImageShare structure models a share
type ImageShare struct {
	ID        uuid.UUID  `json:"id"`
	ImageID   uuid.UUID  `json:"image_id"`
	UserID    uuid.UUID  `json:"user_id"`
	Shared    bool       `json:"shared"`
	CreatedAt types.Date `json:"created_at"`
	UpdatedAt types.Date `json:"updated_at"`
}

// NewImageShare creates a new ImageShare struct
func NewImageShare(image *Image, user *User) *ImageShare {
	return &ImageShare{
		ImageID: image.ID,
		UserID:  user.ID,
		Shared:  true,
	}
}
