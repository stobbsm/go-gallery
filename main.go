package main

import (
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"launchpad.net/go-xdg"

	"github.com/graphql-go/graphql"

	"gitlab.com/stobbsm/go-gallery/api/config"
	"gitlab.com/stobbsm/go-gallery/api/database/postgres"
	"gitlab.com/stobbsm/go-gallery/api/gql"
	"gitlab.com/stobbsm/go-gallery/api/server"
)

func main() {
	// Loadconfig
	var configFile = xdg.Config.Home() + "/gallery/config.json"

	c, err := config.LoadJSON(configFile)
	if err != nil {
		panic(err)
	}

	m, err := postgres.New(postgres.ConnString(c.Database.Host, c.Database.Port, c.Database.Username, c.Database.Password, c.Database.Database))
	if err != nil {
		panic(err)
	}
	defer m.Close()
	root := gql.NewRoot(m)

	sc, err := graphql.NewSchema(
		graphql.SchemaConfig{Query: root.Query, Mutation: root.Mutation},
	)
	if err != nil {
		log.Println("Error creating schema:", err)
	}
	s := server.Server{GqlSchema: &sc}

	router := chi.NewRouter()
	router.Use(
		render.SetContentType(render.ContentTypeJSON), // set content-type headers as application/json
		middleware.Logger,          // log api request calls
		middleware.DefaultCompress, // compress results, mostly gzipping assets and json
		middleware.StripSlashes,    // match paths with a trailing slash, strip it, and continue routing through the mux
		middleware.Recoverer,       // recover from panics without crashing server
	)

	// Create the graphql route with a Server method to handle it
	router.Post("/graphql", s.GraphQL())
	go func() {
		log.Println("Starting graphql server")
		log.Fatal(http.ListenAndServe(":9000", router))
	}()

	// Start the web server for serving vue ui
	log.Println("Starting http fileserver", c.GetPublicPath())
	log.Fatal(http.ListenAndServe(c.GetURL(), http.FileServer(http.Dir(c.GetPublicPath()))))
}
