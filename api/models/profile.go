package models

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// Profile holds special profile information
type Profile struct {
	ID           uuid.UUID  `json:"id"`
	UserID       uuid.UUID  `json:"user_id"`
	BackgroundID uuid.UUID  `json:"background_id"`
	ProfileID    uuid.UUID  `json:"profile_id"`
	CreatedAt    types.Date `json:"created_at"`
	UpdatedAt    types.Date `json:"updated_at"`
}

// NewProfile returns a new profile object
func NewProfile(user *User, background, profile *Image) *Profile {
	return &Profile{
		UserID:       user.ID,
		BackgroundID: background.ID,
		ProfileID:    profile.ID,
	}
}
