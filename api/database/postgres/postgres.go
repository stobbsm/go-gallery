package postgres

import (
	"database/sql"
	"fmt"
	"log"
	"runtime"

	// for wrapping an SQL object
	_ "github.com/lib/pq"
)

// Db wraps a database with helper functions
type Db struct {
	*sql.DB
}

// New creates a new DB connection for the resolver
func New(connString string) (*Db, error) {
	db, err := sql.Open("postgres", connString)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}
	return &Db{db}, nil
}

// ConnString builds a connection string
func ConnString(host string, port int, user, password, dbName string) string {
	connstring := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable binary_parameters=yes", host, port, user, password, dbName)
	log.Println(connstring)
	return connstring
}

// ModelNotFound error type
type ModelNotFound struct {
	err  error
	file string
	line int
}

// NewModelNotFound generates a meaningful error message when
// a model isn't found.
func NewModelNotFound(err error) *ModelNotFound {
	_, file, line, _ := runtime.Caller(1)
	return &ModelNotFound{
		err:  err,
		file: file,
		line: line,
	}
}

// Error fullfills the interface for errors
func (m *ModelNotFound) Error() string {
	return fmt.Sprintf(`Model Not Found Error: %s:%d : %s`, m.file, m.line, m.err)
}

// ModelFound error type
type ModelFound struct {
	err  error
	file string
	line int
}

// NewModelFound generates a meaningful error message when a model is found.
func NewModelFound(err error) *ModelFound {
	_, file, line, _ := runtime.Caller(1)
	return &ModelFound{
		err:  err,
		file: file,
		line: line,
	}
}

// Error fullfills the interface for errors
func (m *ModelFound) Error() string {
	return fmt.Sprintf(`Model Found Error: %s:%d : %s`, m.file, m.line, m.err)
}

// UnknownError is to log unknown errors
type UnknownError struct {
	err  error
	file string
	line int
}

// NewUnknownError generates a meaningful error message
// when an unknown error occurs
func NewUnknownError(err error) *UnknownError {
	_, file, line, _ := runtime.Caller(1)
	return &UnknownError{
		err:  err,
		file: file,
		line: line,
	}
}

// Error fullfills the interface for errors
func (u *UnknownError) Error() string {
	return fmt.Sprintf(`Unknown Error: %s:%d : %s`, u.file, u.line, u.err)
}

// InvalidError error type
type InvalidError struct {
	err  error
	file string
	line int
}

// NewInvalidError generates a meaningful error message when
// a model isn't found.
func NewInvalidError(err error) *InvalidError {
	_, file, line, _ := runtime.Caller(1)
	return &InvalidError{
		err:  err,
		file: file,
		line: line,
	}
}

// Error fullfills the interface for errors
func (m *InvalidError) Error() string {
	return fmt.Sprintf(`Invalid Request: %s:%d : %s`, m.file, m.line, m.err)
}
