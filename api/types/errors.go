package types

import (
	"fmt"
	"runtime"
)

// ParseError is an error used when parsing custome graphql types
type ParseError struct {
	err  error
	file string
	line int
}

// NewParseError creates a new parsing error
func NewParseError(err error) *ParseError {
	_, file, line, _ := runtime.Caller(1)
	return &ParseError{
		err:  err,
		file: file,
		line: line,
	}
}

// Error fullfills the Error interface
func (p *ParseError) Error() string {
	return fmt.Sprintf(`Parsing Error: %s:%d : %s`, p.file, p.line, p.err)
}
