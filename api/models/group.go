package models

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// Group model
type Group struct {
	ID        uuid.UUID  `json:"id"`
	Name      string     `json:"name"`
	OwnerID   uuid.UUID  `json:"owner_id"`
	Members   []User     `json:"members"`
	CreatedAt types.Date `json:"created_at"`
	UpdatedAt types.Date `json:"updated_at"`
}

// NewGroup returns a new Group model
func NewGroup(name string, owner *User) *Group {
	return &Group{
		Name:    name,
		OwnerID: owner.ID,
	}
}
