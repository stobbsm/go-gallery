package gql

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// ProfileGraphqlType is the graphql schema for a Profile
var ProfileGraphqlType graphql.Type

func init() {
	ProfileGraphqlType = graphql.NewObject(graphql.ObjectConfig{
		Name: `Profile`,
		Fields: graphql.FieldsThunk(func() graphql.Fields {
			return graphql.Fields{
				`id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`user_id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`background_id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`profile_id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`created_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
				`updated_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
			}
		}),
	})
}
