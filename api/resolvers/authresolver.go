package resolvers

import (
	"errors"
	"fmt"
	"log"

	"github.com/graphql-go/graphql"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/models"
)

// AuthResolver verifies the auth token
func (r *Resolver) AuthResolver(p graphql.ResolveParams) (interface{}, error) {
	// Requires the users ID to verify
	id, ok := p.Args[`id`].(string)
	if ok {
		uid, err := uuid.FromString(id)
		if err != nil {
			return nil, NewResolverError(err)
		}
		token, ok := p.Args[`token`].(string)
		if ok {
			tid, err := uuid.FromString(token)
			if err != nil {
				return nil, NewResolverError(err)
			}
			if r.db.VerifyToken(uid, tid) {
				return true, nil
			}
		}
	}
	return false, NewResolverError(errors.New(`No resolver found`))
}

// LoginResolver logs in a user and sets the auth token for the session
func (r *Resolver) LoginResolver(p graphql.ResolveParams) (interface{}, error) {
	var foundUser *models.User
	// Check for username
	username, ok := p.Args[`username`].(string)
	if ok {
		log.Println("Found username")
		user, err := r.db.GetUserByUsername(username)
		if err != nil {
			return nil, NewResolverError(err)
		}
		foundUser = user
	}

	// Check for email
	email, ok := p.Args[`email`].(string)
	if ok {
		log.Println("Found email")
		user, err := r.db.GetUserByEmail(email)
		if err != nil {
			return nil, NewResolverError(err)
		}
		foundUser = user
	}

	if foundUser == nil {
		return nil, NoResolverError
	}

	err := r.db.Auth(foundUser, p.Args[`password`].(string))
	if err != nil {
		return nil, NewResolverError(fmt.Errorf(`Authentication error for user: %s`, foundUser.ID.String()))
	}
	return foundUser, nil
}
