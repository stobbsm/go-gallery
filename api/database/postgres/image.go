package postgres

import (
	"errors"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/models"
	q "gitlab.com/stobbsm/go-querybuilder"
)

// Query's needed
var (
	selectAllImage = q.Prepare(
		q.Select(
			`images.id`,
			`images.filename`,
			`images.storagepath`,
			`images.sha256`,
			`images.size`,
			`images.height`,
			`images.width`,
			`images.owner_id`,
			`images.created_at`,
			`images.updated_at`,
		),
	)
	createImageQuery = q.QueryBuilder(
		q.Insert(
			`images`,
			`id`,
			`filename`,
			`storagepath`,
			`sha256`,
			`size`,
			`height`,
			`width`,
			`owner_id`,
			`created_at`,
			`updated_at`,
		),
	)
	getAllImagesQuery = q.QueryBuilder(
		selectAllImage,
		q.From(`images`),
	)
	getImageByIDQuery = q.QueryBuilder(
		selectAllImage,
		q.From(`images`),
		q.WhereEq(`id`),
	)
	getImageByOwnerIDQuery = q.QueryBuilder(
		selectAllImage,
		q.From(`images`),
		q.WhereEq(`owner_id`),
	)
	getImageBySha256Query = q.QueryBuilder(
		selectAllImage,
		q.From(`images`),
		q.WhereEq(`sha256`),
	)
	getImageByOwnerIDSha256 = q.QueryBuilder(
		selectAllImage,
		q.From(`images`),
		q.WhereEq(`owner_id`),
		q.AndEq(`sha256`),
	)
)

// GetImageByID retrieves an image by it's ID
func (d *Db) GetImageByID(id uuid.UUID) (*models.Image, error) {
	i, err := d.GetImageByQuery(getImageByIDQuery, id)
	if len(i) == 0 && err != nil {
		return nil, NewModelNotFound(err)
	}
	if err != nil {
		return nil, NewUnknownError(err)
	}
	return &i[0], nil
}

// GetImagesByOwnerID retrives a list of images based on the given owner_id
func (d *Db) GetImagesByOwnerID(ownerid uuid.UUID) ([]models.Image, error) {
	images, err := d.GetImageByQuery(getImageByOwnerIDQuery, ownerid)
	if err != nil {
		return images, err
	}
	return images, nil
}

// GetImageBySha256 retrieves an image based on it's sha256 sum
func (d *Db) GetImageBySha256(sum string) (*models.Image, error) {
	i, err := d.GetImageByQuery(getImageBySha256Query, sum)
	if len(i) == 0 && err != nil {
		return nil, NewModelNotFound(err)
	}
	if err != nil {
		return nil, NewUnknownError(err)
	}
	return &i[0], nil
}

// GetAllImages returns every image
func (d *Db) GetAllImages() ([]models.Image, error) {
	return d.GetImageByQuery(getAllImagesQuery)
}

// GetImageByQuery retrieves models using a query
func (d *Db) GetImageByQuery(query string, arg ...interface{}) ([]models.Image, error) {
	stmt, err := d.Prepare(query)
	if err != nil {
		return nil, NewUnknownError(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(arg...)
	defer rows.Close()
	if err != nil {
		return nil, NewUnknownError(err)
	}

	var images []models.Image
	var createdat, updatedat time.Time
	for rows.Next() {
		var i models.Image
		err = rows.Scan(
			&i.ID,
			&i.Filename,
			&i.StoragePath,
			&i.Sha256,
			&i.Size,
			&i.Height,
			&i.Width,
			&i.OwnerID,
			&createdat,
			&updatedat,
		)
		if err != nil {
			return images, NewModelNotFound(err)
		}
		i.CreatedAt.Set(createdat)
		i.UpdatedAt.Set(updatedat)

		images = append(images, i)
	}
	return images, nil
}

// checkIfImageExistsDB checks for the existence of an image by sha256sum
// This verifies it exists in the database only, not in the filesystem
func (d *Db) checkIfImageExistsDB(ownerid uuid.UUID, sha256 string) error {
	i, err := d.GetImageByQuery(getImageByOwnerIDSha256, ownerid, sha256)
	if err != nil {
		switch err.(type) {
		case *ModelNotFound:
			return err
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	if len(i) == 0 {
		return NewModelNotFound(errors.New(`Image not found`))
	}
	return NewModelFound(errors.New(`Image found`))
}

// CreateImage generates missing data and inserts an entry in the database
func (d *Db) CreateImage(i *models.Image, data []byte) error {
	stmt, err := d.Prepare(createImageQuery)
	defer stmt.Close()
	if err != nil {
		return NewUnknownError(err)
	}

	if err := i.Process(data); err != nil {
		return NewUnknownError(err)
	}

	if err := d.checkIfImageExistsDB(i.OwnerID, i.Sha256); err != nil {
		switch err := err.(type) {
		case *ModelNotFound:
			log.Println(err)
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	if _, err = stmt.Exec(
		i.ID,
		i.Filename,
		i.StoragePath,
		i.Sha256,
		i.Size,
		i.Height,
		i.Width,
		i.OwnerID,
		i.CreatedAt.GetTime(),
		i.UpdatedAt.GetTime(),
	); err != nil {
		return NewUnknownError(err)
	}

	return nil
}
