package postgres

import (
	"errors"
	"fmt"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/stobbsm/go-gallery/api/models"
	"gitlab.com/stobbsm/go-gallery/api/types"
	q "gitlab.com/stobbsm/go-querybuilder"
)

var (
	prepareSelectUser = q.Prepare(
		q.Select(
			`users.id`,
			`users.name`,
			`users.username`,
			`users.email`,
			`users.token`,
			`users.created_at`,
			`users.updated_at`,
		),
	)
	prepareFromUser  = q.Prepare(q.From(`users`))
	getUserByIDQuery = q.QueryBuilder(
		prepareSelectUser,
		prepareFromUser,
		q.WhereEq(`users.id`),
	)
	getUserByNameQuery = q.QueryBuilder(
		prepareSelectUser,
		prepareFromUser,
		q.WhereEq(`users.username`),
	)
	getUserByEmailQuery = q.QueryBuilder(
		prepareSelectUser,
		prepareFromUser,
		q.WhereEq(`users.email`),
	)
	getPasswordHash = q.QueryBuilder(
		q.Select(`users.password`),
		prepareFromUser,
		q.WhereEq(`users.id`),
	)
	insertUserQuery = q.QueryBuilder(
		q.Insert(
			`users`,
			`id`,
			`name`,
			`username`,
			`password`,
			`email`,
			`token`,
			`created_at`,
			`updated_at`,
		),
	)
	verifyAuthTokenQuery = q.QueryBuilder(
		q.Select(`id`, `token`),
		q.From(`users`),
		q.WhereEq(`id`),
		q.AndEq(`token`),
	)
	attachUserToRoleQuery = q.QueryBuilder(
		q.Insert(`roles_users`, `role_id`, `user_id`, `created_at`, `updated_at`),
	)
	checkUserRoleAttachment = q.QueryBuilder(
		q.Select(`role_id`),
		q.From(`roles_users`),
		q.WhereEq(`user_id`),
		q.AndEq(`role_id`),
	)
	updateUserTokenQuery = q.QueryBuilder(
		q.Update(`users`),
		q.Set(`token`),
		q.WhereEq(`id`),
	)
)

// GetUserByID is used by the database resolver to find users
func (d *Db) GetUserByID(id uuid.UUID) (*models.User, error) {
	u, err := d.GetUserByQuery(getUserByIDQuery, id)
	if err != nil {
		return nil, err
	}
	if len(u) == 0 {
		return nil, NewModelNotFound(errors.New(`User not found`))
	}
	return &u[0], err
}

// GetUsersByID returns a slice of user models from the list of given ids
func (d *Db) GetUsersByID(ids ...uuid.UUID) ([]models.User, error) {
	var users []models.User
	for _, id := range ids {
		u, err := d.GetUserByID(id)
		if err != nil {
			return users, fmt.Errorf("GetUsersByID [d.GetUserByID]: %s", err)
		}
		users = append(users, *u)
	}
	return users, nil
}

// GetUserByUsername returns a user model containing the username
func (d *Db) GetUserByUsername(username string) (*models.User, error) {
	u, err := d.GetUserByQuery(getUserByNameQuery, username)
	if err != nil {
		return nil, err
	}
	if len(u) == 0 {
		return nil, NewModelNotFound(errors.New(`User not found`))
	}
	return &u[0], err
}

// GetUserByEmail returns a user model via the email address
func (d *Db) GetUserByEmail(email string) (*models.User, error) {
	u, err := d.GetUserByQuery(getUserByEmailQuery, email)
	if err != nil {
		return nil, err
	}
	if len(u) == 0 {
		return nil, NewModelNotFound(errors.New(`User not found`))
	}
	return &u[0], err
}

// GetUserByQuery runs the given query with the given argument and returns a user model
func (d *Db) GetUserByQuery(query string, args ...interface{}) ([]models.User, error) {
	stmt, err := d.Prepare(query)
	if err != nil {
		return nil, NewUnknownError(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(args...)
	if err != nil {
		return nil, NewUnknownError(err)
	}
	defer rows.Close()

	var users []models.User
	for rows.Next() {
		var u models.User
		var createdat, updatedat time.Time
		err = rows.Scan(
			&u.ID,
			&u.Name,
			&u.Username,
			&u.Email,
			&u.Token,
			&createdat,
			&updatedat,
		)
		if err != nil {
			return users, NewModelNotFound(err)
		}
		u.CreatedAt.Set(createdat)
		u.UpdatedAt.Set(updatedat)
		users = append(users, u)
	}
	return users, nil
}

// VerifyToken authenticates a token against the authenticated users id
func (d *Db) VerifyToken(id, token uuid.UUID) bool {
	stmt, err := d.Prepare(verifyAuthTokenQuery)
	if err != nil {
		log.Println("VerifyToken [d.Prepare]:", err)
		return false
	}

	rows, err := stmt.Query(id, token)
	if err != nil {
		log.Println("VerifyToken [stmt.Query]:", err)
		return false
	}
	defer rows.Close()
	var dbid, dbtoken uuid.UUID
	for rows.Next() {
		err = rows.Scan(&dbid, &dbtoken)
		if err != nil {
			log.Println("VerifyToken [rows.Scan]:", err)
			return false
		}
		if dbid == id && dbtoken == token {
			return true
		}
	}
	return false
}

// Auth authorizes a user
func (d *Db) Auth(user *models.User, password string) error {
	stmt, err := d.Prepare(getPasswordHash)
	if err != nil {
		return NewUnknownError(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(user.ID)
	if err != nil {
		return NewUnknownError(err)
	}
	defer rows.Close()

	var phash string
	for rows.Next() {
		err = rows.Scan(&phash)
	}
	return bcrypt.CompareHashAndPassword([]byte(phash), []byte(password))
}

func (d *Db) checkIfUserExists(username string) error {
	m, err := d.GetUserByUsername(username)
	if err != nil {
		return NewUnknownError(err)
	}
	if m == nil {
		return NewModelNotFound(errors.New(`Model not found`))
	}
	return NewModelFound(err)
}

// CreateUser a new user in the given database
func (d *Db) CreateUser(u *models.User) error {
	if u.Password == "" {
		return NewInvalidError(errors.New(`Password not set`))
	}

	if err := d.checkIfUserExists(u.Username); err != nil {
		switch err := err.(type) {
		case *ModelNotFound:
			log.Println(err)
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}

	phash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	u.Password = string(phash)

	stmt, err := d.Prepare(insertUserQuery)
	defer stmt.Close()
	if err != nil {
		return NewUnknownError(err)
	}

	u.ID = models.GenUUID()
	u.CreatedAt.Set(time.Now())
	u.UpdatedAt = u.CreatedAt
	u.GenToken()

	_, err = stmt.Exec(
		u.ID,
		u.Name,
		u.Username,
		u.Password,
		u.Email,
		u.Token,
		u.CreatedAt.GetTime(),
		u.UpdatedAt.GetTime(),
	)
	if err != nil {
		return NewUnknownError(err)
	}
	return nil
}

func (d *Db) checkIfUserRoleExists(userid, roleid uuid.UUID) error {
	err := d.CheckUserRole(userid, roleid)
	if err != nil {
		switch err.(type) {
		case *ModelNotFound:
			return err
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	return NewModelFound(nil)
}

// CheckUserRole checks to see if a user has a given role
func (d *Db) CheckUserRole(userid, roleid uuid.UUID) error {
	stmt, err := d.Prepare(checkUserRoleAttachment)
	defer stmt.Close()
	if err != nil {
		return NewUnknownError(err)
	}

	rows, err := stmt.Query(userid, roleid)
	for rows.Next() {
		var r uuid.UUID
		err := rows.Scan(&r)
		if err != nil {
			return NewModelNotFound(errors.New(`User Role not found`))
		}
	}
	return NewModelFound(nil)
}

// AttachUserRole creates a relation between a user and a given role
func (d *Db) AttachUserRole(u *models.User, r *models.Role) error {
	if err := d.checkIfUserRoleExists(u.ID, r.ID); err != nil {
		switch err := err.(type) {
		case *ModelNotFound:
			log.Println(err)
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	stmt, err := d.Prepare(attachUserToRoleQuery)
	defer stmt.Close()
	if err != nil {
		return NewUnknownError(err)
	}

	var ctime, utime types.Date
	ctime.Set(time.Now())
	utime = ctime
	_, err = stmt.Exec(
		r.ID,
		u.ID,
		ctime.GetTime(),
		utime.GetTime(),
	)
	if err != nil {
		return NewUnknownError(err)
	}
	return nil
}

// UpdateUserToken updates a users token
// TODO: Factor using a standard Update method
func (d *Db) UpdateUserToken(uid uuid.UUID, newtoken string) (*models.User, error) {
	stmt, err := d.Prepare(updateUserTokenQuery)
	if err != nil {
		return nil, NewUnknownError(err)
	}

	defer stmt.Close()
	log.Printf(`Uid: '%s' : NewToken '%s'`, uid.String(), newtoken)
	_, err = stmt.Exec(newtoken, uid)
	if err != nil {
		return nil, NewUnknownError(err)
	}
	return d.GetUserByID(uid)
}
