package postgres

import (
	"errors"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/models"
	q "gitlab.com/stobbsm/go-querybuilder"
)

var (
	prepareSelectGroup = q.Prepare(
		q.Select(
			`groups.id`,
			`groups.name`,
			`groups.owner_id`,
			`groups.created_at`,
			`groups.updated_at`,
		),
	)
	prepareFromGroup  = q.Prepare(q.From(`groups`))
	getGroupByIDQuery = q.QueryBuilder(
		prepareSelectGroup,
		prepareFromGroup,
		q.WhereEq(`groups.id`),
	)
	getGroupByNameQuery = q.QueryBuilder(
		prepareSelectGroup,
		prepareFromGroup,
		q.WhereEq(`groups.name`),
	)
	getGroupsByOwnerIDQuery = q.QueryBuilder(
		prepareSelectGroup,
		prepareFromGroup,
		q.WhereEq(`groups.owner_id`),
	)
	getGroupMembersQuery = q.QueryBuilder(
		prepareSelectUser,
		prepareFromUser,
		q.InnerJoin(`groups_users`),
		q.On(`groups.id`, `groups_users.group_id`),
		q.InnerJoin(`groups`),
		q.On(`users.id`, `groups_users.user_id`),
		q.WhereEq(`groups_users.group_id`),
	)
	insertGroupQuery = q.QueryBuilder(
		q.Insert(
			`groups`,
			`id`,
			`name`,
			`owner_id`,
			`created_at`,
			`updated_at`,
		),
	)
)

// GetGroupByID is used by the database resolver to find groups
func (d *Db) GetGroupByID(id uuid.UUID) (*models.Group, error) {
	g, err := d.GetGroupByQuery(getGroupByIDQuery, id)
	if len(g) == 0 || err != nil {
		return nil, NewModelNotFound(err)
	}
	return &g[0], NewModelFound(err)
}

// GetGroupByName retrieves a group by it's name
func (d *Db) GetGroupByName(name string) (*models.Group, error) {
	g, err := d.GetGroupByQuery(getGroupByNameQuery, name)
	if len(g) == 0 || err != nil {
		return nil, NewModelNotFound(err)
	}
	return &g[0], NewModelFound(err)
}

// GetGroupsByOwnerID retrives a list of groups by the owner_id
func (d *Db) GetGroupsByOwnerID(ownerid uuid.UUID) ([]models.Group, error) {
	return d.GetGroupByQuery(getGroupsByOwnerIDQuery, ownerid)
}

// GetGroupMembers retrives a list of users belonging to a group
func (d *Db) GetGroupMembers(gid uuid.UUID) ([]models.User, error) {
	return d.GetUserByQuery(getGroupMembersQuery, gid)
}

// GetGroupByQuery runs the query and returns a group and error
func (d *Db) GetGroupByQuery(query string, arg ...interface{}) ([]models.Group, error) {
	stmt, err := d.Prepare(query)
	if err != nil {
		return nil, NewUnknownError(err)
	}

	rows, err := stmt.Query(arg...)
	if err != nil {
		return nil, NewUnknownError(err)
	}
	defer rows.Close()
	var groups []models.Group
	for rows.Next() {
		var g models.Group
		var ctime, utime time.Time
		err = rows.Scan(
			&g.ID,
			&g.Name,
			&g.OwnerID,
			&ctime,
			&utime,
		)
		if err != nil {
			return nil, NewUnknownError(err)
		}
		g.CreatedAt.Set(ctime)
		g.UpdatedAt.Set(utime)
		groups = append(groups, g)
	}
	return groups, nil
}

func (d *Db) checkIfGroupExists(groupname string) error {
	g, err := d.GetGroupByQuery(getGroupByNameQuery, groupname)
	if err != nil {
		switch err.(type) {
		case *ModelNotFound:
			return err
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	if len(g) == 0 {
		return NewModelNotFound(errors.New(`Group not found`))
	}
	return NewModelFound(nil)
}

// CreateGroup inserts a group into the database
func (d *Db) CreateGroup(g *models.Group) error {
	if err := d.checkIfGroupExists(g.Name); err != nil {
		switch err := err.(type) {
		case *ModelNotFound:
			log.Println(err)
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}

	stmt, err := d.Prepare(insertGroupQuery)
	if err != nil {
		return NewUnknownError(err)
	}
	defer stmt.Close()
	g.ID = models.GenUUID()
	g.CreatedAt.Set(time.Now())
	g.UpdatedAt = g.CreatedAt
	_, err = stmt.Exec(
		g.ID,
		g.Name,
		g.OwnerID,
		g.CreatedAt.GetTime(),
		g.UpdatedAt.GetTime(),
	)
	if err != nil {
		return NewUnknownError(err)
	}
	return nil
}
