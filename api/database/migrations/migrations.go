package migrations

import (
	"database/sql"
	"log"

	"gitlab.com/stobbsm/go-gallery/api/database/postgres"
	q "gitlab.com/stobbsm/go-querybuilder"
)

// Migration contains3 needed parts:
// Name: unique to the migration, identifies the migration in the database
// Up: Serie of SQL statements to run to apply the migration
// Down: Series of SQL statement to reverse the migration
type Migration struct {
	Name string
	Up   []string
	Down []string

	status bool
}

// Migrations holds the migrations in order
type Migrations []Migration

var qInsert = q.QueryBuilder(q.Insert("migrations", "name", "state"))
var qDelete = q.QueryBuilder(q.Delete, q.From("migrations"), q.WhereEq("name"))
var qFind = q.QueryBuilder(q.Select("state"), q.From("migrations"), q.WhereEq("name"))

var createMigrationTable = q.QueryBuilder(
	q.CreateTable("migrations"),
	q.WithColumns(
		q.PrimaryKey(q.VarChar("name", 100)),
		q.Boolean("state"),
	),
)
var dropMigrationTable = q.QueryBuilder(
	q.DropTable("migrations"),
)

// MigrationTableMigration set's up the needed migration table to track migrations
// This needs to be the first Migration added to your
// migrations list.
var MigrationTableMigration = Migration{
	Name: "create_migration_table",
	Up:   []string{createMigrationTable},
	Down: []string{dropMigrationTable},
}

var migrator *Migrations

// GetMigrator returns the migrator instance
func GetMigrator() *Migrations {
	migrator = &Migrations{}
	return migrator
}

// Add a migration to the queue
func (m *Migrations) Add(mig Migration) *Migrations {
	log.Println("Added Migration:", mig.Name)
	if mig.Name != "" && len(mig.Up) > 0 && len(mig.Down) > 0 {
		*m = append(*m, mig)
	} else {
		log.Panicln("Migration incomplete. Must have a name, at least 1 Up, and 1 Down")
	}
	return m
}

func run(db *postgres.Db, query string) (sql.Result, error) {
	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	return stmt.Exec()
}

// Up runs the 'up' migrations
func (m *Migrations) Up(db *postgres.Db) error {
	for _, v := range *m {
		found, err := find(db, v.Name)
		if err != nil {
			log.Println(err)
		}
		if !found {
			log.Println("Bringing Up:", v.Name)
			for _, q := range v.Up {
				_, err := run(db, q)
				if err != nil {
					log.Println(err)
					v.status = false
				} else {
					v.status = true
				}
				record(db, v.Name, v.status)
			}
		}
	}
	return nil
}

// Down runs the 'down' migrations
func (m *Migrations) Down(db *postgres.Db) error {
	c := *m
	for n := range *m {
		v := c[len(*m)-1-n]
		found, err := find(db, v.Name)
		if err != nil {
			log.Println(err)
			return err
		}
		if found {
			log.Println("Bringing Down: ", v.Name)
			for _, q := range v.Down {
				_, err := run(db, q)
				if err != nil {
					return err
				}
				remove(db, v.Name)
			}
		}
	}
	return nil
}

func record(db *postgres.Db, name string, state bool) error {
	stmt, err := db.Prepare(qInsert)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(name, state)
	return err
}

func remove(db *postgres.Db, name string) error {
	stmt, err := db.Prepare(qDelete)
	if err != nil {
		return err
	}
	_, err = stmt.Exec(name)
	return err
}

func find(db *postgres.Db, name string) (bool, error) {
	stmt, err := db.Prepare(qFind)
	if err != nil {
		return false, err
	}
	r, err := stmt.Query(name)
	if err != nil {
		return false, err
	}
	defer r.Close()

	for r.Next() {
		var state bool
		err = r.Scan(&state)
		if err != nil {
			return false, err
		}
		return state, err
	}
	return false, nil
}
