package models

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"image"
	"time"

	// Include jpeg and png support
	_ "image/jpeg"
	_ "image/png"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// Image structure models the same table
type Image struct {
	ID          uuid.UUID  `json:"id"`
	Filename    string     `json:"filename"`
	StoragePath string     `json:"storagepath"`
	Sha256      string     `json:"sha256"`
	Size        int        `json:"size"`
	Height      int        `json:"height"`
	Width       int        `json:"width"`
	OwnerID     uuid.UUID  `json:"owner_id"`
	CreatedAt   types.Date `json:"created_at"`
	UpdatedAt   types.Date `json:"updated_at"`
}

// NewImage creates a new Image model
func NewImage(filename string, owner *User) *Image {
	return &Image{
		Filename: filename,
		OwnerID:  owner.ID,
	}
}

// GenSha256 Generates a sha256 sum of the given data
func (i *Image) genSha256(b []byte) string {
	return fmt.Sprintf("%x", sha256.Sum256(b))
}

// GenPath generates the path name
func (i *Image) genPath() string {
	var dir, file = i.Sha256[:2], i.Sha256[2:]

	return fmt.Sprintf("%s/%s", string(dir), string(file))
}

// Process processes the file and fills metadata
func (i *Image) Process(data []byte) error {
	img, str, err := image.DecodeConfig(bytes.NewReader(data))
	if err != nil {
		return fmt.Errorf("Image.Process [image.DecodeConfig]: %s : %s", str, err)
	}
	i.CreatedAt.Set(time.Now())
	i.UpdatedAt = i.CreatedAt
	i.Height, i.Width = img.Height, img.Width
	i.Size = len(data)
	i.Sha256 = i.genSha256(data)
	i.StoragePath = i.genPath()
	i.ID = GenUUID()

	return nil
}
