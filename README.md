# go-gallery

Photogallery re-write in go.

## Planned Features
- Maintain database of filepaths with metadata
- User Authentication/login
    - Sharing with public links
    - Sharing to sites (facebook, twitter, etc.)
- Add images to gallery
    - Images get uploaded by users and are stored with metadata alongside the rest
    - Users can tag other users, giving them permission to see the image
    - Users can share with friends, but can not share friends photos themselves
- Dynamically resize images for transmission in desired size/format
    - Convert to different image formats (png, jpeg, svg, etc.)
    - Resize original image to requested dimensions
    - Modify saturation, opacity
    - Crop images
    - Do all of this non-destructively
- End-to-end encryption
    - At rest encryption
    - In transit encryption using https
    - Decryption needs to be handled by the browser?
    - Have a "web based viewing mode" that decrypts on demand for transit to browsers

## License
[BSD 3-Clause License](LICENSE)
