package gql

import (
	"github.com/graphql-go/graphql"
)

// LoginQuery defines the login query
func LoginQuery() *graphql.Field {
	return &graphql.Field{
		Type: UserGraphqlType,
		Args: graphql.FieldConfigArgument{
			`username`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			`email`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			`password`: &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: resolver.LoginResolver,
	}
}
