package main

// Command to run the migrations

import (
	"log"
	"os"

	"gitlab.com/stobbsm/go-gallery/api/database/migrations"
	"gitlab.com/stobbsm/go-gallery/api/database/postgres"
	q "gitlab.com/stobbsm/go-querybuilder"
)

func main() {
	var Migrator = migrations.GetMigrator()

	db, err := postgres.New(postgres.ConnString(
		"localhost",
		5432,
		"dev",
		"test123",
		"gallery",
	))
	if err != nil {
		panic(err)
	}

	Migrator.Add(migrations.MigrationTableMigration)

	Migrator.Add(
		migrations.Migration{
			Name: "create_users_table",
			Up: []string{
				q.QueryBuilder(
					q.CreateTable("users"),
					q.WithColumns(
						q.PrimaryKey(q.UUID()),
						q.NotNull(q.VarChar("name", 50)),
						q.Unique(q.VarChar("username", 30)),
						q.Unique(q.VarChar("email", 100)),
						q.VarChar("password", 100),
						q.Field("token", "UUID"),
						q.TimeStamps(),
					),
				),
			},
			Down: []string{
				q.QueryBuilder(
					q.DropTable("users"),
				),
			},
		},
	)

	// Create the Image table
	Migrator.Add(
		migrations.Migration{
			Name: "create_images_table",
			Up: []string{
				q.QueryBuilder(
					q.CreateTable("images"),
					q.WithColumns(
						q.PrimaryKey(q.UUID()),
						q.NotNull(q.VarChar("filename", 255)),
						q.NotNull(q.VarChar("storagepath", 65)),
						q.NotNull(q.VarChar("sha256", 64)),
						q.Field("size", "BIGINT"),
						q.Int("height"),
						q.Int("width"),
						q.ForeignKey("id", "users", q.Field("owner_id", "UUID")),
						q.TimeStamps(),
					),
				),
			},
			Down: []string{
				q.QueryBuilder(q.DropTable("images")),
			},
		},
	)

	// Create Profile table
	Migrator.Add(
		migrations.Migration{
			Name: "create_profiles_table",
			Up: []string{
				q.QueryBuilder(
					q.CreateTable("profiles"),
					q.WithColumns(
						q.PrimaryKey(q.UUID()),
						q.ForeignKey("id", "users", q.Field("user_id", "UUID")),
						q.ForeignKey("id", "images", q.Field("background", "UUID")),
						q.ForeignKey("id", "images", q.Field("profile", "UUID")),
						q.TimeStamps(),
					),
				),
			},
			Down: []string{
				q.QueryBuilder(
					q.DropTable("profiles"),
				),
			},
		},
	)

	// Create the "groups" and "groups_users" tables
	Migrator.Add(
		migrations.Migration{
			Name: "create_groups_table",
			Up: []string{
				q.QueryBuilder(
					q.CreateTable("groups"),
					q.WithColumns(
						q.PrimaryKey(q.UUID()),
						q.NotNull(q.VarChar("name", 50)),
						q.ForeignKey("id", "users", q.Field("owner_id", "UUID")),
						q.TimeStamps(),
					),
				),
				q.QueryBuilder(
					q.CreateTable("groups_users"),
					q.WithColumns(
						q.ForeignKey("id", "groups", q.Field("group_id", "UUID")),
						q.ForeignKey("id", "users", q.Field("member_id", "UUID")),
						q.NotNull(q.TimeStamp("added_at")),
					),
				),
			},
			Down: []string{
				q.QueryBuilder(q.DropTable("groups_users")),
				q.QueryBuilder(q.DropTable("groups")),
			},
		},
	)

	// Create the roles table
	Migrator.Add(
		migrations.Migration{
			Name: "create_roles_table",
			Up: []string{
				q.QueryBuilder(
					q.CreateTable("roles"),
					q.WithColumns(
						q.PrimaryKey(q.UUID()),
						q.NotNull(q.VarChar("name", 20)),
						q.Field("description", "TEXT"),
						q.TimeStamps(),
					),
				),
				q.QueryBuilder(
					q.CreateTable("roles_users"),
					q.WithColumns(
						q.ForeignKey("id", "roles", q.Field("role_id", "UUID")),
						q.ForeignKey("id", "users", q.Field("user_id", "UUID")),
						q.TimeStamps(),
					),
				),
			},
			Down: []string{
				q.QueryBuilder(q.DropTable("roles_users")),
				q.QueryBuilder(q.DropTable("roles")),
			},
		},
	)

	// Create the Image Share table
	Migrator.Add(
		migrations.Migration{
			Name: `create_image_share_table`,
			Up: []string{
				q.QueryBuilder(
					q.CreateTable(`image_share`),
					q.WithColumns(
						q.PrimaryKey(q.UUID()),
						q.ForeignKey(`id`, `images`, q.Field(`image_id`, `UUID`)),
						q.ForeignKey(`id`, `users`, q.Field(`user_id`, `UUID`)),
						q.WithDefault(q.NotNull(q.Boolean(`shared`)), `true`),
						q.TimeStamps(),
					),
				),
			},
			Down: []string{
				q.QueryBuilder(q.DropTable(`image_share`)),
			},
		},
	)

	switch os.Args[1] {
	case "down":
		if err := Migrator.Down(db); err != nil {
			log.Println(err)
		}
	case "up":
		if err := Migrator.Up(db); err != nil {
			log.Println(err)
		}
	default:
		if err := Migrator.Up(db); err != nil {
			log.Println(err)
		}
	}
}
