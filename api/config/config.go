package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

// Config struct holds configuration data
type Config struct {
	Database   Database   `json:"database,omitempty"`
	Filesystem Filesystem `json:"filesystem,omitempty"`
	WebServer  WebServer  `json:"webserver,omitempty"`
}

// Database configuration container
type Database struct {
	Host     string `json:"host,omitempty"`
	Port     int    `json:"port,omitempty"`
	Database string `json:"dbname,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

// Filesystem configuration container
type Filesystem struct {
	Type string `json:"type,omitempty"`
	Path string `json:"path,omitempty"`
}

// WebServer configuration container
type WebServer struct {
	Host string `json:"host,omitempty"`
	Path string `json:"servepath,omitempty"`
	Port int    `json:"serveport,omitempty"`
}

var config *Config

// Get singleton instance of the Config
func Get() *Config {
	config = &Config{}

	return config
}

// Set the Config
func Set(c *Config) {
	config = c
}

// CreateConfig generates a configuration that can be saved
func CreateConfig(dbhost, dbname, dbusername, dbpassword, fstype, fspath, servehost, servepath string, serveport, dbport int) *Config {
	var c = Config{
		Database: Database{
			Host:     dbhost,
			Port:     dbport,
			Database: dbname,
			Username: dbusername,
			Password: dbpassword,
		},
		Filesystem: Filesystem{
			Type: fstype,
			Path: fspath,
		},
		WebServer: WebServer{
			Host: servehost,
			Path: servepath,
			Port: serveport,
		},
	}
	return &c
}

// DumpJSON dumps json config
func (c *Config) DumpJSON() ([]byte, error) {
	return json.MarshalIndent(*c, "", "  ")
}

// GetPublicPath returns the public path under the web server
func (c *Config) GetPublicPath() string {
	return c.WebServer.Path + "/dist"
}

// GetURL returns the full URL of the web server
func (c *Config) GetURL() string {
	return fmt.Sprintf("%s:%d", c.WebServer.Host, c.WebServer.Port)
}

// LoadJSON loads configuration from a given json object
func LoadJSON(configfile string) (*Config, error) {
	c := Get()
	contents, err := ioutil.ReadFile(configfile)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(contents, &c); err != nil {
		return nil, err
	}
	return c, nil
}
