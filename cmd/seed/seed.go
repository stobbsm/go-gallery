package main

// Seed the database with test data

import (
	"io/ioutil"
	"log"

	"gitlab.com/stobbsm/go-gallery/api/config"
	"gitlab.com/stobbsm/go-gallery/api/database/postgres"
	"gitlab.com/stobbsm/go-gallery/api/models"
	"launchpad.net/go-xdg"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	var (
		nobodyUser = models.NewUser(`Nobody`, `nobody`, `nobody@go-gallery.org`, `nobody`)
		adminUser  = models.NewUser(`Administrator`, `admin`, `admin@go-gallery.org`, `admin`)
	)

	c, err := config.LoadJSON(xdg.Config.Home() + "/gallery/config.json")
	if err != nil {
		log.Panic(err)
	}
	d, err := postgres.New(postgres.ConnString(c.Database.Host, c.Database.Port, c.Database.Username, c.Database.Password, c.Database.Database))
	if err != nil {
		log.Panic(err)
	}
	defer d.Close()

	log.Println("Adding admin")
	if err := d.CreateUser(adminUser); err != nil {
		log.Println("Admin User found")
		auser, err := d.GetUserByUsername(adminUser.Username)
		if err != nil {
			log.Panic(err)
		}
		*adminUser = *auser
	}

	log.Println("Adding Nobody")
	if err := d.CreateUser(nobodyUser); err != nil {
		log.Println("Nobody User found")
		nuser, err := d.GetUserByUsername(nobodyUser.Username)
		if err != nil {
			log.Panic(err)
		}
		*nobodyUser = *nuser
	}

	log.Println("Adding image")
	var imagedata = models.NewImage(`/home/stobbsm/Pictures/test.png`, nobodyUser)
	myimage, err := ioutil.ReadFile(imagedata.Filename)
	if err != nil {
		log.Panic(err)
	}
	if err := d.CreateImage(imagedata, myimage); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}

	}

	var (
		profileNobody = models.NewProfile(nobodyUser, imagedata, imagedata)
		profileAdmin  = models.NewProfile(adminUser, imagedata, imagedata)
	)
	log.Println("Adding Nobody Profile")
	if err := d.CreateProfile(profileNobody); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}
	}

	log.Println("Adding Admin Profile")
	if err := d.CreateProfile(profileAdmin); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}
	}

	var (
		adminRole = models.NewRole(`admin`, `Administration role`)
		userRole  = models.NewRole(`user`, `User role`)
	)
	log.Println("Creating Role admin")
	if err := d.CreateRole(adminRole); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}
	}

	log.Println("Creating Role user")
	if err := d.CreateRole(userRole); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}
	}

	log.Println("Attaching Admin user to Admin Role")
	if err := d.AttachUserRole(adminUser, adminRole); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}
	}

	var (
		adminGroup  = models.NewGroup(`admin`, adminUser)
		nobodyGroup = models.NewGroup(`nobody`, nobodyUser)
	)
	log.Println("Creating Group admin")
	if err := d.CreateGroup(adminGroup); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}
	}

	log.Println("Creating Group user")
	if err := d.CreateGroup(nobodyGroup); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}
	}

	var shareImage = models.NewImageShare(imagedata, adminUser)
	log.Println(`Sharing Nobodies image with Admin`)
	if err := d.ShareImage(shareImage); err != nil {
		switch err.(type) {
		case *postgres.ModelFound:
			log.Println(err)
		default:
			log.Panic(err)
		}
	}
}
