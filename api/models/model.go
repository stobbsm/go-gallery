package models

import (
	uuid "github.com/satori/go.uuid"
)

// GenUUID is used to create a new UUID easily and consistently.
func GenUUID() uuid.UUID {
	return uuid.NewV4()
}
