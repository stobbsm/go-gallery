package gql

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// ImageGraphqlType is the graphql schema for an image
var ImageGraphqlType graphql.Type

func init() {
	ImageGraphqlType = graphql.NewObject(graphql.ObjectConfig{
		Name: `Image`,
		Fields: graphql.FieldsThunk(func() graphql.Fields {
			return graphql.Fields{
				`id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`filename`: &graphql.Field{
					Type: graphql.String,
				},
				`sha256`: &graphql.Field{
					Type: graphql.String,
				},
				`size`: &graphql.Field{
					Type: graphql.Int,
				},
				`height`: &graphql.Field{
					Type: graphql.Int,
				},
				`width`: &graphql.Field{
					Type: graphql.Int,
				},
				`owner_id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`created_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
				`updated_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
			}
		}),
	})
}

// ImageQuery retrieves images based on multiple query types
func ImageQuery() *graphql.Field {
	return &graphql.Field{
		Type: ImageGraphqlType,
		Args: graphql.FieldConfigArgument{
			`id`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			`sha256`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: resolver.ImageResolver,
	}
}

// ImagesQuery returns a list of images
func ImagesQuery() *graphql.Field {
	return &graphql.Field{
		Type: graphql.NewList(ImageGraphqlType),
		Args: graphql.FieldConfigArgument{
			`owner_id`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			`all`: &graphql.ArgumentConfig{
				Type: graphql.Boolean,
			},
		},
		Resolve: resolver.ImagesResolver,
	}
}
