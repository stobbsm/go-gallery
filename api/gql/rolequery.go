package gql

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// RoleGraphqlType contains the graphql schema for a role
var RoleGraphqlType graphql.Type

func init() {
	RoleGraphqlType = graphql.NewObject(graphql.ObjectConfig{
		Name: `Role`,
		Fields: graphql.FieldsThunk(func() graphql.Fields {
			return graphql.Fields{
				`id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`name`: &graphql.Field{
					Type: graphql.String,
				},
				`description`: &graphql.Field{
					Type: graphql.String,
				},
				`members`: &graphql.Field{
					Type: graphql.NewList(UserGraphqlType),
				},
				`created_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
				`updated_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
			}
		}),
	})
}
