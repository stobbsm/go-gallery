module gitlab.com/stobbsm/go-gallery

go 1.12

require (
	github.com/99designs/gqlgen v0.10.1
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/agnivade/levenshtein v1.0.2 // indirect
	github.com/alexflint/go-arg v1.1.0 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/creack/pty v1.1.9 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/fsnotify/fsnotify v1.4.7 // indirect
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-siris/siris v7.4.0+incompatible // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gogo/protobuf v1.3.0 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/graphql-go/graphql v0.7.8
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/labstack/echo v3.3.10+incompatible // indirect
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.2.0
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	github.com/mdempsky/unconvert v0.0.0-20190921185256-3ecd357795af // indirect
	github.com/mitchellh/mapstructure v1.1.2 // indirect
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/oxequa/interact v0.0.0-20171114182912-f8fb5795b5d7 // indirect
	github.com/oxequa/realize v2.0.2+incompatible // indirect
	github.com/rs/cors v1.7.0 // indirect
	github.com/satori/go.uuid v1.2.0
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/urfave/cli v1.22.1 // indirect
	github.com/valyala/fasttemplate v1.1.0 // indirect
	github.com/vektah/dataloaden v0.3.0 // indirect
	github.com/vektah/gqlparser v1.1.2
	gitlab.com/stobbsm/go-querybuilder v0.4.4
	golang.org/x/crypto v0.0.0-20191107222254-f4817d981bb6
	golang.org/x/net v0.0.0-20191011234655-491137f69257 // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20191012152004-8de300cfc20a // indirect
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/urfave/cli.v2 v2.0.0-20190806201727-b62605953717 // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
	launchpad.net/go-xdg v0.0.0-20140208094800-000000000010
	launchpad.net/gocheck v0.0.0-20140225173054-000000000087 // indirect
	sourcegraph.com/sourcegraph/appdash v0.0.0-20190731080439-ebfcffb1b5c0 // indirect
)
