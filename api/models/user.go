package models

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// User holds the user model
type User struct {
	ID        uuid.UUID  `json:"id"`
	Name      string     `json:"name"`
	Username  string     `json:"username"`
	Email     string     `json:"email"`
	Password  string     `json:"password"`
	Token     uuid.UUID  `json:"token"`
	CreatedAt types.Date `json:"created_at"`
	UpdatedAt types.Date `json:"updated_at"`
}

// NewUser returns a raw User model
func NewUser(name, username, email, password string) *User {
	return &User{
		Name:     name,
		Username: username,
		Email:    email,
		Password: password,
	}
}

// GenToken creates a new token as a V4UUID
func (u *User) GenToken() {
	u.Token = uuid.NewV4()
}
