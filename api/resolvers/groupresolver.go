package resolvers

import (
	"errors"

	"github.com/graphql-go/graphql"
	uuid "github.com/satori/go.uuid"
)

// GroupResolver find groups with given parameters
func (r *Resolver) GroupResolver(p graphql.ResolveParams) (interface{}, error) {
	// Get By ID
	id, ok := p.Args[`id`].(string)
	if ok {
		gid, err := uuid.FromString(id)
		if err != nil {
			return nil, NewResolverError(err)
		}
		group, err := r.db.GetGroupByID(gid)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return group, nil
	}

	// Get By Name
	name, ok := p.Args[`name`].(string)
	if ok {
		group, err := r.db.GetGroupByName(name)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return group, nil
	}

	// Get By Owner ID
	ownerid, ok := p.Args[`owner_id`].(string)
	if ok {
		uid, err := uuid.FromString(ownerid)
		if err != nil {
			return nil, NewResolverError(err)
		}
		groups, err := r.db.GetGroupsByOwnerID(uid)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return groups, nil
	}
	return nil, NewResolverError(errors.New(`No resolve method found`))
}
