package gql

import (
	"fmt"

	"github.com/graphql-go/graphql"
	"gitlab.com/stobbsm/go-gallery/api/database/postgres"
	"gitlab.com/stobbsm/go-gallery/api/resolvers"
)

var resolver *resolvers.Resolver

// ExecuteQuery runs our graphql queries
func ExecuteQuery(query string, schema graphql.Schema) *graphql.Result {
	result := graphql.Do(graphql.Params{
		Schema:        schema,
		RequestString: query,
	})

	// Error check
	if len(result.Errors) > 0 {
		fmt.Printf(`Unexpected errors inside ExecuteQuery: %v`, result.Errors)
	}

	return result
}

// Root schema for graphql server
type Root struct {
	Query    *graphql.Object
	Mutation *graphql.Object
}

// NewRoot creates a new root query for graphql
func NewRoot(db *postgres.Db) *Root {
	resolver = resolvers.New(db)
	root := Root{
		Query: graphql.NewObject(
			graphql.ObjectConfig{
				Name: `Query`,
				Fields: graphql.Fields{
					`user`:   UserQuery(),
					`login`:  LoginQuery(),
					`image`:  ImageQuery(),
					`images`: ImagesQuery(),
					`groups`: GroupQuery(),
				},
			},
		),
		Mutation: graphql.NewObject(
			graphql.ObjectConfig{
				Name: `Mutation`,
				Fields: graphql.Fields{
					`newuser`:  AddUserMutation(),
					`newtoken`: UpdateUserToken(),
				},
			},
		),
	}
	return &root
}
