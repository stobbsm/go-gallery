package resolvers

import (
	"errors"
	"log"

	"github.com/graphql-go/graphql"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/models"
)

// UserResolver resolves a user by username
func (r *Resolver) UserResolver(p graphql.ResolveParams) (interface{}, error) {
	username, ok := p.Args[`username`].(string)
	if ok {
		user, err := r.db.GetUserByUsername(username)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return user, nil
	}

	id, ok := p.Args[`id`].(string)
	if ok {
		guid, err := uuid.FromString(id)
		if err != nil {
			return nil, NewResolverError(err)
		}
		user, err := r.db.GetUserByID(guid)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return user, nil
	}
	return nil, NewResolverError(errors.New(`No valid resolvers found`))
}

// UserGroupsResolver gets user groups
func (r *Resolver) UserGroupsResolver(p graphql.ResolveParams) (interface{}, error) {
	user := p.Source.(*models.User)
	groups, err := r.db.GetGroupsByOwnerID(user.ID)
	if err != nil {
		return nil, NewResolverError(err)
	}
	return groups, nil
}

// UserImagesResolver gets a users images
func (r *Resolver) UserImagesResolver(p graphql.ResolveParams) (interface{}, error) {
	user := p.Source.(*models.User)
	images, err := r.db.GetImagesByOwnerID(user.ID)
	if err != nil {
		return nil, NewResolverError(err)
	}
	return images, nil
}

// UserRolesResolver gets a users roles
func (r *Resolver) UserRolesResolver(p graphql.ResolveParams) (interface{}, error) {
	user := p.Source.(*models.User)
	roles, err := r.db.GetUserRoles(user.ID)
	if err != nil {
		return nil, NewResolverError(err)
	}
	return roles, nil
}

// AddUserMutation adds a user with the given graphql query
func (r *Resolver) AddUserMutation(p graphql.ResolveParams) (interface{}, error) {
	user := models.NewUser(
		p.Args[`name`].(string),
		p.Args[`username`].(string),
		p.Args[`email`].(string),
		p.Args[`password`].(string),
	)
	err := r.db.CreateUser(user)
	if err != nil {
		return nil, NewResolverError(err)
	}
	return user, nil
}

// UpdateUserToken updates the given user with a new token
func (r *Resolver) UpdateUserToken(p graphql.ResolveParams) (interface{}, error) {
	var foundUser *models.User
	id, ok := p.Args[`id`].(string)
	if ok {
		gid, err := uuid.FromString(id)
		if err != nil {
			return nil, NewResolverError(err)
		}
		user, err := r.db.GetUserByID(gid)
		if err != nil {
			return nil, NewResolverError(err)
		}
		foundUser = user
	}

	username, ok := p.Args[`username`].(string)
	if ok {
		user, err := r.db.GetUserByUsername(username)
		if err != nil {
			return nil, NewResolverError(err)
		}
		foundUser = user
	}
	if !ok {
		return nil, NoResolverError
	}
	user, err := r.db.UpdateUserToken(foundUser.ID, uuid.NewV4().String())
	if err != nil {
		return nil, NewResolverError(err)
	}
	log.Println(`Found User Token:`, user.Token)
	return user, nil
}
