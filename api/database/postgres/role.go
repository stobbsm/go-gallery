package postgres

import (
	"errors"
	"fmt"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/models"
	q "gitlab.com/stobbsm/go-querybuilder"
)

// Queries
var (
	prepareSelectRole = q.Prepare(
		q.Select(
			"roles.id",
			"roles.name",
			"roles.description",
			"roles.created_at",
			"roles.updated_at",
		),
	)
	prepareFromRoles = q.Prepare(
		q.From("roles"),
	)
	prepareSelectRoleMembers = q.Prepare(
		q.Select("roles_users.user_id"),
	)
	prepareFromRoleRelationUsers = q.Prepare(
		q.From("roles, roles_users"),
	)
	getRoleByIDQuery = q.QueryBuilder(
		prepareSelectRole,
		prepareFromRoles,
		q.WhereEq("id"),
	)
	getRoleByNameQuery = q.QueryBuilder(
		prepareSelectRole,
		prepareFromRoles,
		q.WhereEq("name"),
	)
	getRoleByUserIDQuery = q.QueryBuilder(
		prepareSelectRole,
		q.From(`roles`),
		q.InnerJoin(`roles_users`),
		q.On(`roles.id`, `roles_users.role_id`),
		q.InnerJoin(`users`),
		q.On(`users.id`, `roles_users.user_id`),
		q.WhereEq(`roles_users.user_id`),
	)
	insertRoleQuery = q.QueryBuilder(
		q.Insert(
			`roles`,
			`id`,
			`name`,
			`description`,
			`created_at`,
			`updated_at`,
		),
	)
)

// GetRoleByID retrieves a role with it's user ID's
func (d *Db) GetRoleByID(id uuid.UUID) (*models.Role, error) {
	r, err := d.GetRoleByQuery(getRoleByIDQuery, id)
	if len(r) == 0 || err != nil {
		return nil, NewModelNotFound(err)
	}
	return &r[0], NewModelFound(err)
}

// GetRoleByName finds a role using it's name
func (d *Db) GetRoleByName(name string) (*models.Role, error) {
	r, err := d.GetRoleByQuery(getRoleByNameQuery, name)
	if len(r) == 0 || err != nil {
		return nil, NewModelNotFound(err)
	}
	return &r[0], NewModelFound(err)
}

// GetUserRoles returns a list of roles held by a user ID
func (d *Db) GetUserRoles(userid uuid.UUID) ([]models.Role, error) {
	return d.GetRoleByQuery(getRoleByUserIDQuery, userid)
}

// GetRoleByQuery runs a query to get models
func (d *Db) GetRoleByQuery(query string, arg ...interface{}) ([]models.Role, error) {
	stmt, err := d.Prepare(query)
	defer stmt.Close()
	if err != nil {
		return nil, NewUnknownError(err)
	}

	rows, err := stmt.Query(arg...)
	defer rows.Close()
	if err != nil {
		return nil, NewUnknownError(err)
	}
	defer rows.Close()

	var roles []models.Role

	for rows.Next() {
		var r models.Role
		var createdat, updatedat time.Time
		err = rows.Scan(
			&r.ID,
			&r.Name,
			&r.Description,
			&createdat,
			&updatedat,
		)
		if err != nil {
			return roles, NewModelNotFound(err)
		}
		r.CreatedAt.Set(createdat)
		r.UpdatedAt.Set(updatedat)
		roles = append(roles, r)
	}

	return roles, nil
}

func (d *Db) checkIfRoleExists(name string) error {
	r, err := d.GetRoleByQuery(getRoleByNameQuery, name)
	if err != nil {
		switch err.(type) {
		case *ModelNotFound:
			return err
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	if len(r) == 0 {
		return NewModelNotFound(errors.New(`Role not found`))
	}
	return NewModelFound(nil)
}

// CreateRole sets the remaining fields and create's a new Role
func (d *Db) CreateRole(r *models.Role) error {
	if err := d.checkIfRoleExists(r.Name); err != nil {
		switch err := err.(type) {
		case *ModelNotFound:
			log.Println(err)
		case *ModelFound:
			return err
		default:
			return NewUnknownError(err)
		}
	}
	stmt, err := d.Prepare(insertRoleQuery)
	if err != nil {
		return fmt.Errorf("CreateRole [d.Prepare]: %s", err)
	}
	defer stmt.Close()

	r.ID = models.GenUUID()
	r.CreatedAt.Set(time.Now())
	r.UpdatedAt = r.CreatedAt

	_, err = stmt.Exec(
		r.ID,
		r.Name,
		r.Description,
		r.CreatedAt.GetTime(),
		r.UpdatedAt.GetTime(),
	)
	if err != nil {
		return fmt.Errorf(`CreateRole [stmt.Exec]: %s`, err)
	}
	return nil
}
