package resolvers

import (
	"github.com/graphql-go/graphql"
	uuid "github.com/satori/go.uuid"
)

// RoleResolver finds roles with given information
func (r *Resolver) RoleResolver(p graphql.ResolveParams) (interface{}, error) {
	// Find by ID
	id, ok := p.Args[`id`].(string)
	if ok {
		rid, err := uuid.FromString(id)
		if err != nil {
			return nil, NewResolverError(err)
		}

		role, err := r.db.GetRoleByID(rid)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return role, nil
	}

	// Find by Name
	name, ok := p.Args[`name`].(string)
	if ok {
		role, err := r.db.GetRoleByName(name)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return role, nil
	}
	return nil, NoResolverError
}
