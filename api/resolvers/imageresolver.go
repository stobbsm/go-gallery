package resolvers

import (
	"errors"

	"github.com/graphql-go/graphql"
	uuid "github.com/satori/go.uuid"
)

// ImageResolver resolves a single image
func (r *Resolver) ImageResolver(p graphql.ResolveParams) (interface{}, error) {
	// Get by ID
	id, ok := p.Args["id"].(string)
	if ok {
		image, err := r.db.GetImageByID(uuid.FromStringOrNil(id))
		if err != nil {
			return nil, NewResolverError(err)
		}
		return image, nil
	}
	// Get by Sha256
	shasum, ok := p.Args["sha256"].(string)
	if ok {
		image, err := r.db.GetImageBySha256(shasum)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return image, nil
	}
	return nil, NewResolverError(errors.New(`No valid resolvers found`))
}

// ImagesResolver returns a list of images
func (r *Resolver) ImagesResolver(p graphql.ResolveParams) (interface{}, error) {
	ownerid, ok := p.Args["owner_id"].(string)
	if ok {
		guid, err := uuid.FromString(ownerid)
		if err != nil {
			return nil, NewResolverError(err)
		}

		images, err := r.db.GetImagesByOwnerID(guid)
		if err != nil {
			return nil, NewResolverError(err)
		}
		return images, nil
	}
	all, ok := p.Args[`all`].(bool)
	if ok {
		if !all {
			return nil, NewResolverError(errors.New(`all was false. What's the point?`))
		}
		images, err := r.db.GetAllImages()
		if err != nil {
			return nil, NewResolverError(err)
		}
		return images, nil
	}
	return nil, NewResolverError(errors.New(`No valid resolvers found`))
}
