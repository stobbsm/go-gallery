package gql

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// UserGraphqlType Controls the Schema for Users
var UserGraphqlType graphql.Type

func init() {
	UserGraphqlType = graphql.NewObject(graphql.ObjectConfig{
		Name: `User`,
		Fields: graphql.FieldsThunk(func() graphql.Fields {
			return graphql.Fields{
				`id`: &graphql.Field{
					Type: graphql.NewNonNull(types.CustomIDType),
				},
				`name`: &graphql.Field{
					Type: graphql.String,
				},
				`username`: &graphql.Field{
					Type: graphql.String,
				},
				`email`: &graphql.Field{
					Type: graphql.String,
				},
				`token`: &graphql.Field{
					Type: types.TokenType,
				},
				`created_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
				`updated_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
				`images`: &graphql.Field{
					Type:    graphql.NewList(ImageGraphqlType),
					Resolve: resolver.UserImagesResolver,
				},
				`groups`: &graphql.Field{
					Type:    graphql.NewList(GroupGraphqlType),
					Resolve: resolver.UserGroupsResolver,
				},
				`roles`: &graphql.Field{
					Type:    graphql.NewList(RoleGraphqlType),
					Resolve: resolver.UserRolesResolver,
				},
				`profile`: &graphql.Field{
					Type: ProfileGraphqlType,
				},
			}
		}),
	})
}

// UserQuery defines queries for users
func UserQuery() *graphql.Field {
	return &graphql.Field{
		Type: UserGraphqlType,
		Args: graphql.FieldConfigArgument{
			"username": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			"id": &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: resolver.UserResolver,
	}
}

// AddUserMutation is used to add a new user
func AddUserMutation() *graphql.Field {
	return &graphql.Field{
		Type:        UserGraphqlType,
		Description: `Create new user`,
		Args: graphql.FieldConfigArgument{
			`username`: &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			`name`: &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			`email`: &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
			`password`: &graphql.ArgumentConfig{
				Type: graphql.NewNonNull(graphql.String),
			},
		},
		Resolve: resolver.AddUserMutation,
	}
}

// UpdateUserToken updates a user token
func UpdateUserToken() *graphql.Field {
	return &graphql.Field{
		Type:        UserGraphqlType,
		Description: `Updated user auth token`,
		Args: graphql.FieldConfigArgument{
			`id`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			`username`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: resolver.UpdateUserToken,
	}
}
