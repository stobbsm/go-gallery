package resolvers

import (
	"errors"
	"fmt"
	"runtime"

	"gitlab.com/stobbsm/go-gallery/api/database/postgres"
)

// Resolver struct holds a database connection
type Resolver struct {
	db *postgres.Db
}

// New returns a new resolver
func New(db *postgres.Db) *Resolver {
	return &Resolver{db: db}
}

// ResolverError is a type for resolver errors
type ResolverError struct {
	err error
}

// NewResolverError generates a resolver error message
func NewResolverError(err error) *ResolverError {
	var r = ResolverError{
		err: err,
	}

	return &r
}

func (r *ResolverError) Error() string {
	_, file, line, _ := runtime.Caller(1)
	return fmt.Sprintf(`Resolver Error: %s:%d : %s`, file, line, r.err)
}

// NoResolverError is a default message when resolvers can't be found
var NoResolverError = NewResolverError(errors.New(`No valid resolvers found`))
