package models

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// Role is used for access control
type Role struct {
	ID          uuid.UUID  `json:"id"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	CreatedAt   types.Date `json:"created_at"`
	UpdatedAt   types.Date `json:"updated_at"`
}

// NewRole returns a new role model
func NewRole(name, description string) *Role {
	return &Role{
		Name:        name,
		Description: description,
	}
}
