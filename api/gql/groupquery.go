package gql

import (
	"github.com/graphql-go/graphql"
	"gitlab.com/stobbsm/go-gallery/api/types"
)

// GroupGraphqlType defines the structure of the Group object
// Graphql Schema
var GroupGraphqlType graphql.Type

func init() {
	GroupGraphqlType = graphql.NewObject(graphql.ObjectConfig{
		Name: `Group`,
		Fields: graphql.FieldsThunk(func() graphql.Fields {
			return graphql.Fields{
				`id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`name`: &graphql.Field{
					Type: graphql.String,
				},
				`owner_id`: &graphql.Field{
					Type: types.CustomIDType,
				},
				`members`: &graphql.Field{
					Type: graphql.NewList(types.CustomIDType),
				},
				`created_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
				`updated_at`: &graphql.Field{
					Type: types.GraphqlDateType,
				},
			}
		}),
	})
}

// GroupQuery finds groups with 3 different identifications:
// id: The ID of the group, as a UUID
// owner_id: The ID of the user that owns the group
// name: The actual name of the group
func GroupQuery() *graphql.Field {
	return &graphql.Field{
		Type: GroupGraphqlType,
		Args: graphql.FieldConfigArgument{
			`id`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			`owner_id`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
			`name`: &graphql.ArgumentConfig{
				Type: graphql.String,
			},
		},
		Resolve: resolver.GroupResolver,
	}
}
