package types

import (
	"errors"
	"log"
	"time"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

// Date type for use with go-gallery
type Date struct {
	t time.Time
}

// GetTime returns the underlying time object
func (d *Date) GetTime() time.Time {
	return d.t
}

// Set returns the underlying time object
func (d *Date) Set(t time.Time) {
	d.t = t
}

// GraphqlDateType Defines the graphql scalar definition for Dates
var GraphqlDateType = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "Date",
	Description: "Custom date type",
	Serialize: func(value interface{}) interface{} {
		switch value := value.(type) {
		case Date:
			v := &value
			return v.GetTime().String()
		case *Date:
			return value.GetTime().String()
		default:
			return nil
		}
	},
	ParseValue: func(value interface{}) interface{} {
		var d Date
		switch value := value.(type) {
		case time.Time:
			d.Set(value)
			return d
		case *time.Time:
			d.Set(*value)
			return d
		default:
			return NewParseError(errors.New(`No date to parse`))
		}
	},
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			t, err := time.Parse(time.RFC3339, valueAST.Value)
			if err != nil {
				log.Println(NewParseError(err))
			}
			var d Date
			d.Set(t)
			return d
		default:
			return NewParseError(errors.New(`Unable to parse given value`))
		}
	},
})
