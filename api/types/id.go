package types

import (
	"errors"
	"log"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
	uuid "github.com/satori/go.uuid"
)

// CustomIDType is used so graphql actually parses and uses UUID style strings
var CustomIDType = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "ID",
	Description: "UUID for ID type",
	Serialize: func(value interface{}) interface{} {
		switch value := value.(type) {
		case uuid.UUID:
			return value.String()
		case *uuid.UUID:
			v := *value
			return v.String()
		default:
			return NewParseError(errors.New(`No value to parse`))
		}
	},
	ParseValue: func(value interface{}) interface{} {
		switch value := value.(type) {
		case string:
			id, err := uuid.FromString(value)
			if err != nil {
				log.Println(NewParseError(err))
			}
			return id
		case *string:
			s := *value
			id, err := uuid.FromString(s)
			if err != nil {
				log.Println("Error parsing UUID string", err)
			}
			return id
		default:
			return NewParseError(errors.New(`No value to parse`))
		}
	},
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			id, err := uuid.FromString(valueAST.Value)
			if err != nil {
				log.Println(NewParseError(err))
			}
			return id
		default:
			return NewParseError(errors.New(`No value to parse`))
		}
	},
})
