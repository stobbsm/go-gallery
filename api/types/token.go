package types

import (
	"errors"
	"log"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
	uuid "github.com/satori/go.uuid"
)

// TokenType is used to store user auth tokens, which are basically
// just UUIDs
var TokenType = graphql.NewScalar(graphql.ScalarConfig{
	Name:        `Token`,
	Description: `Auth token for Users`,
	Serialize: func(value interface{}) interface{} {
		switch value := value.(type) {
		case uuid.UUID:
			return value.String()
		case *uuid.UUID:
			v := *value
			return v.String()
		default:
			return NewParseError(errors.New(`Value is not in uuid format`))
		}
	},
	ParseValue: func(value interface{}) interface{} {
		switch value := value.(type) {
		case string:
			token, err := uuid.FromString(value)
			if err != nil {
				log.Println(NewParseError(err))
			}
			return token
		case *string:
			token, err := uuid.FromString(*value)
			if err != nil {
				log.Println("Error parsing UUID string", err)
			}
			return token
		default:
			return NewParseError(errors.New(`Value is not in uuid format`))
		}
	},
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			id, err := uuid.FromString(valueAST.Value)
			if err != nil {
				log.Println(NewParseError(err))
			}
			return id
		default:
			return NewParseError(errors.New(`Value is not in uuid format`))
		}
	},
})
